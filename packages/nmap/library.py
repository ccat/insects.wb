# -*- coding: utf-8 -*-

VERSION=1

import json
import subprocess
import select
from datetime import datetime
import pytz

def loadConfig():
    return json.load(open("config.json"))

def execute(cmdList,procClass=None):
    p = subprocess.Popen (cmdList,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return CMDprocess(p,procClass)

class CMDout:
    def __init__(self,outType="stdout",outLine=""):
        self.outType=outType
        self.outLine=outLine

    def json(self):
        jsonObj={"datatype":"log"}
        if(self.outType=="stdout"):
            jsonObj["status"]="info"
        else:
            jsonObj["status"]="error"
        jsonObj["date"]=datetime.now(pytz.utc).strftime("%Y/%m/%d %H:%M:%S %Z")
        jsonObj["output"]=self.outLine
        return json.dumps(jsonObj)

class CMDproc:
    def __init__(self):
        self.outputs=""
        self.result={"datatype":"result"}

    def stdout(self,obj):
        self.outputs+=obj.outLine
        print obj.json()
        self.processStdout(obj)

    def stderr(self,obj):
        self.outputs+=obj.outLine
        print obj.json()
        self.processStderr(obj)

    def finalize(self,retcode):
        self.processFinalize(retcode)

    def processStdout(self,obj):
        pass

    def processStderr(self,obj):
        pass

    def processFinalize(self,retcode):
        pass

    def json(self):
        return json.dumps(self.result)

class CMDprocess:
    def __init__(self,process,procClass=None):
        self.process=process
        self.rfhd=dict(stdout=process.stdout, stderr=process.stderr)
        self.retcode=None
        self.procClass=procClass
        if(self.procClass==None):
            self.procClass=CMDproc()

    def wait(self):
        while self.retcode==None:
            self.isFinished()
            target="a"
            while target:
                target=self.getOutput()
                if(target):
                    if(target.outType=="stdout"):
                        self.procClass.stdout(target)
                    else:
                        self.procClass.stderr(target)
        self.procClass.finalize(self.retcode)

    def getOutput(self):
        if not self.rfhd:
            return None
        rready, _, _=select.select(self.rfhd.values(), [ ], [ ])
        for name,fh in self.rfhd.items():
            if fh in rready:
                line=fh.readline()
                if line=='':
                    fh.close()
                    del self.rfhd[name]
                else:
                    return CMDout(name, line.strip("\n"))
        return None

    def isFinished(self):
        self.retcode = self.process.poll()
        if self.retcode is not None:
            return True
        return False
