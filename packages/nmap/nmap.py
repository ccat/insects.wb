#! /usr/bin/python
# -*- coding: utf-8 -*-

#import sys
import library
from xml.etree import ElementTree

class NMAPclass(library.CMDproc):
    def __init__(self,resultFilename):
        #super(library.CMDproc, self).__init__()
        library.CMDproc.__init__(self)
        self.resultFilename=resultFilename
        self.result["tcp port"]=[["Port,State","Service/Product Version(extra)","Script"]]

    def processFinalize(self,retcode):
        resultXML=ElementTree.parse(self.resultFilename)
        portList=resultXML.findall(".//port")
        for item in portList:
            if(item.get("portid")==None):
                continue
            tempItem=[]
            tempItem.append(item.get("portid"))
            script=""
            for child in list(item):
                if(child.tag=="state"):
                    tempItem[0]=tempItem[0]+"/tcp, "+child.get("state","")
                elif(child.tag=="service"):
                    tempS=child.get("name")+"/"+child.get("product","")
                    if(child.get("version")):
                        tempS=tempS+" "+child.get("version")
                    if(child.get("extrainfo")):
                        tempS=tempS+"("+child.get("extrainfo")+")"
                    tempItem.append(tempS)
                elif(child.tag=="script"):
                    if(child.get("output")):
                        script=script+child.get("output")+".\n"
            tempItem.append(script)
            self.result["tcp port"].append(tempItem)


    """
    def processStdout(self,obj):
        if(obj.outLine.find("Host is up")!=-1):
            self.result["host status"]="running"
        if(obj.outLine.find("/tcp")!=-1 and obj.outLine.find("Discovered")!=0 and obj.outLine.find("TRACEROUTE")!=0):
            tempS=obj.outLine.split(" ")
            port=None
            tempD={"status":None,"service":None,"version":""}
            for item in tempS:
                if(port==None):
                    port=item.split("/")[0]
                elif(tempD["status"]==None):
                    if(item==""):
                        continue
                    tempD["status"]=item
                elif(tempD["service"]==None):
                    if(item==""):
                        continue
                    tempD["service"]=item
                else:
                    if(item=="" and tempD["version"]!=""):
                        tempD["version"]+=" "
                    else:
                        if(tempD["version"]!=""):
                            tempD["version"]+=" "
                        tempD["version"]+=item
            self.result["tcp port"][port]=tempD
    """


def main():
    config=library.loadConfig()
    resultClass=NMAPclass("nmapresult.xml")
    nmapObj=library.execute(["nmap","-A","-v","-p",unicode(config["start_port"])+"-"+unicode(config["end_port"]),"-oX",resultClass.resultFilename,config["target"]],resultClass)
    nmapObj.wait()
    print resultClass.json()

main()
